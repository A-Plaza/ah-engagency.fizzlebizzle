﻿


$(document).ready(function () {

    checkIfBazz();
    $("#result").hide();
    //**Events**
    $('#bazz-toggle').on('change', function () {

        checkIfBazz();

    });
    $("#generate-btn").on("click", function () {

        generateFZBZ();
    });


    //Actions
    function checkIfBazz() {

        if ($('#bazz-toggle').is(":checked")) {
            $("#predicate").show();
            $("#bazz").show();
        }
        else {
            $("#predicate").val("NA");
            $("#bazz").val("");
            $("#predicate").hide();
            $("#bazz").hide();
        }

    };

    function generateFZBZ() {

        var fzbzData =
            {
                Start: $("#start").val(),
                End: $("#end").val(),
                Fizz: $("#fizz").val(),
                Buzz: $("#buzz").val(),
                Bazz: $("#bazz").val(),
                IsBazz: $("#bazz-toggle").is(":checked"),
                Predicate: $('#predicate').find(":selected").val()
            };
        console.log(fzbzData);
        if (fzbzData.Start > 0 && fzbzData.End > 0 && fzbzData.Fizz > 0 && fzbzData.Buzz > 0) {

            $.ajax({
                url: '/FizzleBizzle/Generate',
                data: fzbzData,
                type: "POST",
                dataType: 'json',
            }).done(function (result) {

                $("#result").text(result);
                console.log(result);
                $("#result").show();
            });

        }
        

    };
});