﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzleBizzle.ViewModels
{
    public class FizzleBizzle_ViewModel
    {
        public int Start { get; set; }
        public int End { get; set; }
        public int Fizz { get; set; }
        public int Buzz { get; set; }
        public int Bazz { get; set; }
        public bool IsBazz { get; set; }
        public string Predicate { get; set; }
        public string[] result { get; set; }
    }
}