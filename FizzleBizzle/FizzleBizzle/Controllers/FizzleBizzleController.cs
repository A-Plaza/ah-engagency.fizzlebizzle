﻿using FizzleBizzle.ViewModels;
using FizzleBizzle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FizzleBizzle.Controllers
{
    public class FizzleBizzleController : Controller
    {
        // GET: FizzleBizzle
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Generate(FizzleBizzle_ViewModel model)
        {
            FZBZ fzbz = new FZBZ();
            fzbz.SetFizz(model.Fizz);
            fzbz.SetBuzz(model.Buzz);
            if (model.IsBazz)
            {
                //fzbz.FizzBuzzBazz(model.Start, model.End)
            }
            //this should be the result of the generate
            return Json(fzbz.GenerateResult(model), JsonRequestBehavior.AllowGet);
        }
    }
}