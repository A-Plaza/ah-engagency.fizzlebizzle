﻿using FizzleBizzle.Interfaces;
using FizzleBizzle.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzleBizzle.Models
{
    public class FZBZ : IFizzleBizzle
    {
        public int Fizz { set; private get; }
        public int Buzz { set; private get; }

        public void SetFizz(int fizz)
        {
            Fizz = fizz;
        }
        public void SetBuzz(int buzz)
        {
            Buzz = buzz;
        }

        public string[] FizzBuzz(int start, int end)
        {
            int arrSize = (end - start) + 1;
            string[] fizzBuzz = new string[arrSize];
            bool isFizz;
            bool isBuzz;
            bool isFizzBuzz;
            for (int i = 0; i < fizzBuzz.Length; i++)
            {
                isBuzz = ((i + start) % Buzz == 0);
                isFizz = ((i + start) % Fizz == 0);
                isFizzBuzz = (isFizz && isBuzz);

                if (isFizz && isBuzz)
                {
                    fizzBuzz[i] = "FizzBuzz";
                }
                else if (isFizz)
                {
                    fizzBuzz[i] = "Fizz";
                }
                else if (isBuzz)
                {
                    fizzBuzz[i] = "Buzz";
                }
                else
                {
                    fizzBuzz[i] = (i + start).ToString();
                }
               
            }
            return fizzBuzz;
        }

        public string[] FizzBuzzBazz(int start, int end, Predicate<int> bazzPredicate)
        {
            int arrSize = (end - start) + 1;
            string[] fizzBuzzBazz = new string[arrSize];
            bool isFizz;
            bool isBuzz;
            bool isFizzBuzz;
            for (int i = 0; i < fizzBuzzBazz.Length; i++)
            {
                isFizz = ((i + start) % Fizz == 0);
                isBuzz = ((i + start) % Buzz == 0);
                isFizzBuzz = (isFizz && isBuzz);

                if (isFizz && isBuzz && bazzPredicate(i+1))
                {
                    fizzBuzzBazz[i] = "FizzBuzzBazz";
                }
                else if (isFizz && isBuzz)
                {
                    fizzBuzzBazz[i] = "FizzBuzz";
                }
                else if (isFizz)
                {
                    fizzBuzzBazz[i] = "Fizz";
                }
                else if (isBuzz)
                {
                    fizzBuzzBazz[i] = "Buzz";
                }
                else
                {
                    fizzBuzzBazz[i] = (i + start).ToString();
                }
            }

            return fizzBuzzBazz;
        }

        public string[] GenerateResult(FizzleBizzle_ViewModel fzbzModel)
        {
            string[] result = null;
            if (fzbzModel.IsBazz && fzbzModel.Bazz > 0 && fzbzModel.Predicate != "NA")
            {
                
                switch (fzbzModel.Predicate)
                {
                    case ">":
                        result = FizzBuzzBazz(fzbzModel.Start, fzbzModel.End, CompareGreaterThan(fzbzModel.Bazz));
                        break;
                    case "<":
                        result = FizzBuzzBazz(fzbzModel.Start, fzbzModel.End, CompareLessThan(fzbzModel.Bazz));
                        break;
                    case "==":
                        result = FizzBuzzBazz(fzbzModel.Start, fzbzModel.End, CompareEqualTo(fzbzModel.Bazz));
                        break;
                }
            }
            else
            {
                result = FizzBuzz(fzbzModel.Start, fzbzModel.End);
            }

            return result;
        }

        public Predicate<int> CompareGreaterThan(int bazz)
        {
            Predicate<int> pre = delegate (int x) { return x > bazz; };
            return pre;
        }

        public Predicate<int> CompareLessThan(int bazz)
        {
            Predicate<int> pre = delegate (int x) { return x < bazz; };
            return pre;
        }

        public Predicate<int> CompareEqualTo(int bazz)
        {
            Predicate<int> pre = delegate (int x) { return x == bazz; };
            return pre;
        }
    }
}